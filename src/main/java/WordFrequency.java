public class WordFrequency {
    private String value;
    private int count;

    public WordFrequency(String word, int frequency) {
        this.value = word;
        this.count = frequency;
    }


    public String getValue() {
        return this.value;
    }

    public int getWordCount() {
        return this.count;
    }


}
