import java.util.*;
import java.util.stream.Collectors;

public class WordFrequencyGame {//todo long method

    public String getResult(String sentence) {
        String sentenceSplitter = "\\s+";
        String calculateError = "Calculate Error";
        try {
            //split the input string with 1 to n pieces of spaces
            String[] words = sentence.split(sentenceSplitter);
            List<WordFrequency> wordFrequencyList = generateWordFrequencyList(words);
            sortWordFrequencyList(wordFrequencyList);
            return formatWordFrequencyList(wordFrequencyList);
        } catch (Exception exception) {
            return calculateError;
        }

    }

    private List<WordFrequency> generateWordFrequencyList(String[] words) {
        Map<String, Long> wordCountMap = Arrays.stream(words)
                .collect(Collectors.groupingBy(word -> word, Collectors.counting()));

        return wordCountMap.entrySet().stream()
                .map(entry -> new WordFrequency(entry.getKey(), entry.getValue().intValue()))
                .collect(Collectors.toList());
    }

    private void sortWordFrequencyList(List<WordFrequency> wordFrequencyList) {
        wordFrequencyList.sort((word1, word2) -> word2.getWordCount() - word1.getWordCount());
    }

    private String formatWordFrequencyList(List<WordFrequency> wordFrequencyList) {
        String joiner = "\n";
        return wordFrequencyList.stream()
                .map(wordFrequency -> String.format("%s %d", wordFrequency.getValue(), wordFrequency.getWordCount()))
                .collect(Collectors.joining(joiner));
    }
}
